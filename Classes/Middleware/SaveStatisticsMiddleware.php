<?php
namespace Kit\T3Stats\Middleware;

use Psr\Http\Message\ResponseInterface;
use Psr\Http\Message\ServerRequestInterface;
use Psr\Http\Server\MiddlewareInterface;
use Psr\Http\Server\RequestHandlerInterface;
use TYPO3\CMS\Core\Utility\GeneralUtility;
use TYPO3\CMS\Core\Database\ConnectionPool;
use TYPO3\CMS\Extbase\Utility\DebuggerUtility;

class SaveStatisticsMiddleware implements MiddlewareInterface {

    /**
     * SaveStatisticsMiddleware
     *
     * \TYPO3\CMS\Core\Http\RequestHandlerInterface -> $handler
     *
     * @param \Psr\Http\Message\ServerRequestInterface $request
     * @param  $handler
     * @return mixed
     */
    public function process(ServerRequestInterface $request, RequestHandlerInterface $handler): ResponseInterface
    {
        // Set Timezone stuff
        date_default_timezone_set('Europe/Berlin');
        setlocale(LC_ALL, 'de_DE.utf8');

        /** @var \TYPO3\CMS\Core\Http\NormalizedParams $normalizedParams */
        $normalizedParams = $request->getAttribute('normalizedParams');

        // Invoke inner middlewares and eventually the TYPO3 kernel
        $response = $handler->handle($request);
        $uri = $request->getUri();
        $headers = $request->getHeaders();
        $remoteAdr = $normalizedParams->getRemoteAddress();
        $serverParams = $request->getServerParams();
#DebuggerUtility::var_dump($this);
        $aInsert = [
            'hash' => md5(sprintf("%s%s", $headers['user-agent'][0], $remoteAdr)),
            'remote' => $remoteAdr,
            'host' => $headers['host'][0] ? $headers['host'][0] : "---",
            'path' => $uri->getPath(),
            'user_agent' => $headers['user-agent'][0] ? $headers['user-agent'][0] : "---",
            'tstamp' => time(),
            'user_date' => strftime("%F %T")
        ];

        if ( $this->scanPing($headers['user-agent'][0]) ) {
            $this->storePing($aInsert);
        }

        return $response;
    }

    /**
     * scanPing
     *
     * @param string $sUserAgent
     * @return bool
     */
    private function scanPing($sUserAgent="") {
        $bReturn = TRUE;

        $aCleanPhrase = [
            "phpservermon"
        ];

        foreach ( $aCleanPhrase as $sPhrase ) {
            if ( strstr( strtolower($sUserAgent), strtolower($sPhrase) ) ) {
                $bReturn = FALSE;
            }
        }

        return $bReturn;
    }

    /**
     * storePing
     *
     * @param array $aInsert
     * @return void
     */
    private function storePing($aInsert=array()) {
        GeneralUtility::makeInstance(ConnectionPool::class)->getConnectionForTable('tx_log')->insert(
            'tx_log',
            $aInsert
        );
    }
}