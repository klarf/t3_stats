<?php

namespace Kit\T3Stats\Controller;


use TYPO3\CMS\Backend\View\BackendTemplateView;
use TYPO3\CMS\Core\Database\ConnectionPool;
use TYPO3\CMS\Core\Utility\DebugUtility;
use TYPO3\CMS\Core\Utility\GeneralUtility;
use TYPO3\CMS\Extbase\Mvc\View\ViewInterface;
use TYPO3\CMS\Extbase\Utility\DebuggerUtility;
use Kit\T3Stats\Service\StatisticsService;

class T3StatisticsController extends \TYPO3\CMS\Extbase\Mvc\Controller\ActionController {

    /** @var StatisticsService */
    protected $Statistics;

    /**
     * @var \TYPO3\CMS\Core\Page\PageRenderer
     */
    protected $pageRenderer;

    protected function initializeView(ViewInterface $view) {

        parent::initializeView($view);
        if ($this->actionMethodName == 'indexAction'
            || $this->actionMethodName == 'listAction'
            || $this->actionMethodName == 'howAction') {


            #$this->generateMenu();
            #$this->registerDocheaderButtons();
            #$view->getModuleTemplate()->setFlashMessageQueue($this->controllerContext->getFlashMessageQueue());
        }
        if ($view instanceof BackendTemplateView) {
            $pageRenderer = $view->getModuleTemplate()->getPageRenderer();
            $pageRenderer->loadRequireJsModule('TYPO3/CMS/Backend/Modal');
            $pageRenderer->loadRequireJsModule('TYPO3/CMS/Backend/MultiStepWizard');
            $pageRenderer->loadRequireJsModule('TYPO3/CMS/Backend/Icons');
        }
    }

    protected function listAction() {
        /** @var StatisticsService $Statistics */
        $Statistics = GeneralUtility::makeInstance(StatisticsService::class);
        $logArray = $Statistics->getAllHits();

        $this->view->assign("logArray", $logArray);
    }

    protected function showAction() {
        $arguments = $this->request->getArguments();

        /** @var StatisticsService $Statistics */
        $Statistics = GeneralUtility::makeInstance(StatisticsService::class);
        $logArray = $Statistics->getHitsByDate($arguments['dateId']);

        $this->view->assign("arguments", $arguments);
        $this->view->assign("logArray", $logArray);
    }

}