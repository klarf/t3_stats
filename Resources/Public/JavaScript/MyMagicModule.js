define(['jquery'], function($) {
    var MyMagicModule = {
        foo: 'bar'
    };

    MyMagicModule.init = function() {
        console.log("######");
        console.log("ACTION");
        console.log("######");

        $("#t3stats-table").on('click', function(){
            console.warn("FCK!");
            console.log(Chart);
        });
    };

    // To let the module be a dependency of another module, we return our object
    return MyMagicModule.init();
});