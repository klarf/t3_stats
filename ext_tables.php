<?php
defined('TYPO3_MODE') || die('Access denied.');

\TYPO3\CMS\Extbase\Utility\ExtensionUtility::registerModule(
    'Kit.T3Stats',
    'tools',
    'tx_t3stats',
    'bottom',
    [
        'T3Statistics' => 'list, show'
    ],
    [
        'access' => 'admin',
        'icon' => 'EXT:t3_stats/Resources/Public/Icons/module-stats.svg',
        'labels' => 'LLL:EXT:t3_stats/Resources/Private/Language/locallang_mod.xlf'
    ]
);