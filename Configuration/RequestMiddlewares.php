<?php

use Kit\T3Stats\Middleware\AddMetatagMiddleware;

return [
    'frontend' => [
        'kit/t3stats/savestats' => [
            'target' => \Kit\T3Stats\Middleware\SaveStatisticsMiddleware::class,
            'after' => [
                'typo3/cms-frontend/eid'
            ],
            'before' => [
                'typo3/cms-frontend/tsfe'
            ]
        ]
    ]
];
