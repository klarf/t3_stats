#
# Add SQL definition of database tables
#

CREATE TABLE tx_log (
   uid int(11) NOT NULL auto_increment,

   tstamp int(11) DEFAULT '0' NOT NULL,
   user_date varchar (20) DEFAULT '' NOT NULL,

   hash varchar(100) DEFAULT '' NOT NULL,
   remote varchar(30) DEFAULT '' NOT NULL,
   host varchar(100) DEFAULT '' NOT NULL,
   path varchar(300) DEFAULT '' NOT NULL,
   user_agent varchar(600) DEFAULT '' NOT NULL,

   PRIMARY KEY (uid),
   KEY crdate (tstamp)
);