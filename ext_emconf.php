<?php

/**
 * Extension Manager/Repository config file for ext "kit_stats".
 */
$EM_CONF[$_EXTKEY] = [
    'title' => 'KIT T3 Statistics',
    'description' => 'Klar IT Webconsulting Extension for Statistics',
    'category' => 'templates',
    'constraints' => [
        'depends' => [
            'typo3' => '9.5.0-10.4.99'
        ],
        'conflicts' => [
        ],
    ],
    'autoload' => [
        'psr-4' => [
            'Kit\\T3Stats\\' => 'Classes',
        ],
    ],
    'state' => 'stable',
    'uploadfolder' => 0,
    'createDirs' => '',
    'clearCacheOnLoad' => 1,
    'author' => 'Frank Klar',
    'author_email' => 'typo3@klar-it.info',
    'author_company' => 'Klar IT Webconsulting',
    'version' => '1.1.2',
];
